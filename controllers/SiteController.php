<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Pacientes;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use app\models\Auxiliares;
use app\models\Telefonos;
use app\models\Deposiciones;
use app\models\Revisiones;
use app\models\Rellenados;
use app\models\Menstruaciones;
use app\models\Partes;
use app\models\Habitaciones;
use app\models\Camas;





class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
     public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
       $this->layout = 'logins'; // Asignar el layout específico

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);

    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    
public function actionMostrarpacientes()
{
    $dataProvider = new ActiveDataProvider([
        'query' => Pacientes::find()->orderBy(['nombreCompleto' => SORT_ASC]), // Ordena por nombre completo en orden ascendente
        'pagination' => [
            'pageSize' => 9, // Ajusta según sea necesario
        ],
    ]);

    return $this->render('pacientes2', [
        'dataProvider' => $dataProvider,
    ]);
}

    
    public function actionRegistros()
    {
    return $this->render('registros'); // Este es el nombre de la vista en el archivo 'registros.php'
    }
    
public function actionVerprotocolos($pdfName) {
    // Ruta al directorio donde se almacenan los archivos PDF
    $pdfDirectory = Yii::getAlias('@webroot') . '/documentos/';

    // Ruta completa al archivo PDF
    $pdfPath = $pdfDirectory . $pdfName;

    // Verificar si el archivo existe
    if (file_exists($pdfPath)) {
        // Devolver el contenido del PDF al navegador
        return Yii::$app->response->sendFile($pdfPath, $pdfName, ['inline' => true]);
    } else {
        // Manejar el caso en que el archivo no existe
        throw new \yii\web\NotFoundHttpException('El archivo PDF no se encuentra.');
    }
}

public function actionVerprotocolorcp() {
    return $this->actionVerprotocolos('rcp.pdf');
}

public function actionVerprotocoloatragantamiento() {
    return $this->actionVerprotocolos('Atragantamiento.pdf');
}

public function actionVerprotocolocontencion() {
    return $this->actionVerprotocolos('Protocolo-contencion.pdf');
}

public function actionVerprotocolomovilizacion() {
    return $this->actionVerprotocolos('Movilizacion.pdf');
}


  public function actionProtocolos()
    {
    return $this->render('protocolos'); // Este es el nombre de la vista en el archivo 'registros.php'
    }
    
     public function actionTecnicos()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Auxiliares::find()->with('telefonos'),
            'pagination' => [
            'pageSize' => 10, // Número de registros por página
        ],
        ]);

        return $this->render('tecnicos', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
//    public function actionEditarpaciente($id)
//{
//    $model = $this->findModel($id);
//
//    if ($model->load(Yii::$app->request->post()) && $model->save()) {
//        return $this->redirect(['view', 'id' => $model->id]);
//    }
//
//    return $this->render('edit', [
//        'model' => $model,
//    ]);
//}
    
public function actionRegistrospartes($pacienteId) {
    Yii::info("ID del paciente: $pacienteId", 'app.controller');

    // Obtener el primer y último día del mes actual
    $startDate = date('Y-m-01');
    $endDate = date('Y-m-t');

    // Buscar el modelo del paciente
    $pacienteModel = Pacientes::findOne($pacienteId);

    // Obtener los DataProviders para los registros de deposiciones filtrados por el mes actual
    $dataProviderDeposiciones = new ActiveDataProvider([
        'query' => Deposiciones::find()
            ->where(['idPacientes' => $pacienteId])
            ->andWhere(['>=', 'fecha', $startDate])
            ->andWhere(['<=', 'fecha', $endDate]),
    ]);

    // Obtener los DataProviders para los registros de menstruaciones filtrados por el mes actual
    $dataProviderMenstruaciones = new ActiveDataProvider([
        'query' => Menstruaciones::find()
            ->where(['idPacientes' => $pacienteId])
            ->andWhere(['>=', 'fecha', $startDate])
            ->andWhere(['<=', 'fecha', $endDate]),
    ]);

    // Obtener los DataProviders para los registros de revisiones filtrados por el mes actual
    $dataProviderRevisiones = new ActiveDataProvider([
        'query' => Revisiones::find()
            ->where(['idPacientes' => $pacienteId])
            ->andWhere(['>=', 'fechaManos', $startDate])
            ->andWhere(['<=', 'fechaManos', $endDate])
            ->andWhere(['>=', 'fechaPies', $startDate])
            ->andWhere(['<=', 'fechaPies', $endDate]),
    ]);

// Obtener los DataProviders para los registros de partes filtrados por el mes actual
$dataProviderPartes = new ActiveDataProvider([
    'query' => Partes::find()
        ->select(['partes.id', 'rellenados.fecha', 'rellenados.turno', 'partes.descripcion'])
        ->innerJoinWith('rellenados')
        ->where(['rellenados.idPacientes' => $pacienteId])
        ->andWhere(['>=', 'rellenados.fecha', $startDate])
        ->andWhere(['<=', 'rellenados.fecha', $endDate]),
]);

    // Renderizar la vista y pasar los DataProviders junto con el modelo del paciente
    return $this->render('registrosPartes', [
        'model' => $pacienteModel,
        'dataProviderDeposiciones' => $dataProviderDeposiciones,
        'dataProviderMenstruaciones' => $dataProviderMenstruaciones,
        'dataProviderRevisiones' => $dataProviderRevisiones,
        'dataProviderPartes' => $dataProviderPartes,
        'pacienteId' => $pacienteId,
    ]);
}



    
public function actionCreardeposicion($pacienteId)
{
    // Buscar el modelo del paciente
    $pacienteModel = Pacientes::findOne($pacienteId);

    // Crear una nueva instancia del modelo de Deposiciones
    $model = new Deposiciones();
    $model->idPacientes = $pacienteId;

    // Determinar la hora actual
    $horaActual = date('H');

    // Definir el valor predeterminado para el campo "turno" en función de la hora actual
    if ($horaActual >= 8 && $horaActual < 15) {
        $turnoPredeterminado = 'Mañana';
    } elseif ($horaActual >= 15 && $horaActual < 22) {
        $turnoPredeterminado = 'Tarde';
    } else {
        $turnoPredeterminado = 'Noche';
    }

    // Verificar si se ha enviado el formulario y se ha guardado correctamente el modelo
    if ($model->load(Yii::$app->request->post())) {
        // Formatear la fecha antes de guardar
        $model->fecha = date('Y-m-d', strtotime($model->fecha));

        if ($model->save()) {
            Yii::$app->session->setFlash('success', 'Deposición creada correctamente.');
            // Redirigir a la vista de detalles del paciente con el ID del paciente
            return $this->redirect(['site/registrospartes', 'pacienteId' => $pacienteId]);
        }
    }

    // Renderizar el formulario con el modelo de deposiciones y el modelo del paciente
    return $this->render('formulariodeposiciones', [
        'model' => $model,
        'nombrePaciente' => $pacienteModel->nombreCompleto, // Pasar el nombre del paciente a la vista
        'turnoPredeterminado' => $turnoPredeterminado, // Pasar el turno predeterminado a la vista
    ]);
}



    
 public function actionCrearmenstruacion($pacienteId)
{
    $pacienteModel = Pacientes::findOne($pacienteId);
    $model = new Menstruaciones(['idPacientes' => $pacienteId]);

    if ($model->load(Yii::$app->request->post())) {
        // Verificar si se seleccionó la opción positiva o negativa
        if ($model->positivo !== null) {
            // Convertir la fecha al formato adecuado para la base de datos
            $model->fecha = Yii::$app->formatter->asDate($model->fecha, 'yyyy-MM-dd');

            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Menstruación creada correctamente.');
                return $this->redirect(['site/registrospartes', 'pacienteId' => $pacienteId]);
            }
        } else {
            Yii::$app->session->setFlash('error', 'Debe seleccionar si la menstruación es positiva o negativa.');
        }
    }

    return $this->render('formularioMenstruaciones', [
        'model' => $model,
        'nombrePaciente' => $pacienteModel->nombreCompleto,
    ]);
}



public function actionCrearrevision($pacienteId)
{
    $pacienteModel = Pacientes::findOne($pacienteId);
    $model = new Revisiones();
    $model->idPacientes = $pacienteId;

    if ($model->load(Yii::$app->request->post())) {
        $model->fechaManos = date('Y-m-d');
        $model->fechaPies = date('Y-m-d');
        if ($model->save()) {
            Yii::$app->session->setFlash('success', 'Revisión creada correctamente.');
            return $this->redirect(['site/registrospartes', 'pacienteId' => $pacienteId]);
        }
    }

    return $this->render('formularioRevisiones', [
        'model' => $model,
        'nombrePaciente' => $pacienteModel->nombreCompleto,
        'pacienteId' => $pacienteId,
    ]);
}
public function actionCrearparte($pacienteId)
{
    // Buscar el modelo del paciente
    $pacienteModel = Pacientes::findOne($pacienteId);
    if (!$pacienteModel) {
        throw new NotFoundHttpException('El paciente no existe.');
    }

    // Crear nuevas instancias de los modelos de Partes y Rellenados
    $model = new Partes();
    $modelRellenado = new Rellenados();
    $auxiliares = Auxiliares::find()->all();

    // Determinar la fecha de hoy y la hora actual
    $fechaHoy = date('Y-m-d');
    $horaActual = date('H');

    // Definir el valor predeterminado para el campo "turno" en función de la hora actual
    if ($horaActual >= 8 && $horaActual < 15) {
        $turnoPredeterminado = 'Mañana';
    } elseif ($horaActual >= 15 && $horaActual < 22) {
        $turnoPredeterminado = 'Tarde';
    } else {
        $turnoPredeterminado = 'Noche';
    }

    // Manejar la solicitud POST
    if ($model->load(Yii::$app->request->post()) && $modelRellenado->load(Yii::$app->request->post())) {
        // Formatear la fecha correctamente antes de guardar
        $modelRellenado->fecha = $fechaHoy;
        $modelRellenado->turno = $turnoPredeterminado;

        if ($model->save()) {
            $idPartes = $model->id;
            $modelRellenado->idPacientes = $pacienteId;
            $modelRellenado->idPartes = $idPartes;
            if ($modelRellenado->save()) {
                Yii::$app->session->setFlash('success', 'Parte creado correctamente.');
                return $this->redirect(['site/registrospartes', 'pacienteId' => $pacienteId]);
            }
        }

        Yii::$app->session->setFlash('error', 'Error al guardar los datos.');
    }

    // Renderizar el formulario con los datos necesarios
    return $this->render('formulariopartes', [
        'model' => $model,
        'modelRellenado' => $modelRellenado,
        'nombrePaciente' => $pacienteModel->nombreCompleto,
        'pacienteId' => $pacienteId,
        'auxiliares' => $auxiliares,
        'turnoPredeterminado' => $turnoPredeterminado,
    ]);
}




    // Acción para mostrar el historial de deposiciones
    public function actionHistorialdeposiciones($pacienteId)
    {
        $pacienteModel = Pacientes::findOne($pacienteId);
        $dataProviderDeposiciones = new ActiveDataProvider([
            'query' => Deposiciones::find()->where(['idPacientes' => $pacienteId]),
        ]);

        return $this->render('historialDeposiciones', [
            'model' => $pacienteModel,
            'dataProviderDeposiciones' => $dataProviderDeposiciones,
            'pacienteId' => $pacienteId,
        ]);
    }

    // Acción para mostrar el historial de menstruaciones
public function actionHistorialmenstruaciones($pacienteId)
{
    $pacienteModel = Pacientes::findOne($pacienteId);
    
    $dataProviderMenstruaciones = new ActiveDataProvider([
        'query' => Menstruaciones::find()->where(['idPacientes' => $pacienteId]),
    ]);

    return $this->render('historialMenstruaciones', [
        'model' => $pacienteModel,
        'dataProviderMenstruaciones' => $dataProviderMenstruaciones,
        'pacienteId' => $pacienteId,
    ]);
}


 
// Acción para mostrar el historial de revisiones
public function actionHistorialrevisiones($pacienteId)
{
    $pacienteModel = Pacientes::findOne($pacienteId);
    
    $dataProviderRevisiones = new ActiveDataProvider([
        'query' => Revisiones::find()->where(['idPacientes' => $pacienteId]),
    ]);

    return $this->render('historialRevisiones', [
        'model' => $pacienteModel,
        'dataProviderRevisiones' => $dataProviderRevisiones, // Asegúrate de pasar esta variable a la vista
        'pacienteId' => $pacienteId,
    ]);
}


    // Acción para mostrar el historial de partes
public function actionHistorialpartes($pacienteId) {
    // Buscar el modelo del paciente
    $pacienteModel = Pacientes::findOne($pacienteId);

    // Obtener los DataProviders para los registros de partes
    $dataProviderPartes = new ActiveDataProvider([
        'query' => Partes::find()->innerJoinWith('rellenados')->where(['rellenados.idPacientes' => $pacienteId]),
    ]);

    // Renderizar la vista y pasar los DataProviders junto con el modelo del paciente
    return $this->render('historialPartes', [
        'model' => $pacienteModel,
        'dataProviderPartes' => $dataProviderPartes,
        'pacienteId' => $pacienteId, // Asegúrate de incluir esta línea si la necesitas en la vista
    ]);
}


public function actionTodasdeposiciones()
{
      // Obtener todas las alertas de menstruaciones
    $dataProvider = new ActiveDataProvider([
        'query' => Deposiciones::find(),
        'pagination' => [
            'pageSize' => 10, // Número de registros por página
        ],
    ]);

    return $this->render('todasDeposiciones', [
        'dataProvider' => $dataProvider,
    ]);
}




public function actionMostraralertas()
{
    return $this->render('alertas');
}



public function actionAlertasenema()
{
    // Obtener la fecha actual y la fecha hace 4 días
    $fechaActual = date('Y-m-d');
    $fechaHaceCuatroDias = date('Y-m-d', strtotime('-4 days'));

    // Array para almacenar las alertas de Enema
    $alertasEnema = [];

    // Consultar las deposiciones de cada paciente desde hace 4 días hasta ayer
    $deposiciones = Deposiciones::find()
        ->select(['idPacientes', 'fecha', 'positivo'])
        ->andWhere(['<', 'fecha', $fechaActual])
        ->andWhere(['>=', 'fecha', $fechaHaceCuatroDias])
        ->orderBy(['idPacientes' => SORT_ASC, 'fecha' => SORT_DESC])
        ->all();

    // Array asociativo para mantener el conteo de días consecutivos sin deposiciones positivas para cada paciente
    $diasSinDeposiciones = [];

    foreach ($deposiciones as $deposicion) {
        if ($deposicion->positivo == 0) {
            // Si la deposición es negativa, incrementar el contador de días consecutivos sin deposiciones positivas
            if (!isset($diasSinDeposiciones[$deposicion->idPacientes])) {
                $diasSinDeposiciones[$deposicion->idPacientes] = 1;
            } else {
                $diasSinDeposiciones[$deposicion->idPacientes]++;
            }
        } else {
            // Si la deposición es positiva, reiniciar el contador de días consecutivos sin deposiciones positivas
            $diasSinDeposiciones[$deposicion->idPacientes] = 0;
        }

        // Verificar si el paciente ha tenido 4 días consecutivos sin deposiciones positivas
        if ($diasSinDeposiciones[$deposicion->idPacientes] >= 4) {
            // Agregar el nombre del paciente a las alertas de Enema
            $paciente = Pacientes::findOne($deposicion->idPacientes);
            $alertasEnema[] = $paciente->nombreCompleto;
        }
    }

    // Renderizar la vista 'alertasEnema' y pasar los datos a la vista
    return $this->render('alertasEnema', [
        'alertasEnema' => $alertasEnema,
    ]);
}



public function actionTodasmenstruaciones()
{
     // Obtener todas las alertas de menstruaciones
    $dataProvider = new ActiveDataProvider([
        'query' => Menstruaciones::find(),
        'pagination' => [
            'pageSize' => 10, // Número de registros por página
        ],
    ]);

    return $this->render('todasMenstruaciones', [
        'dataProvider' => $dataProvider,
    ]);
}


public function actionAlertasmenstruaciones()
{
    // Obtener la fecha actual
    $fechaActual = date('Y-m-d');

    // Array para almacenar las alertas de menstruaciones
    $alertasMenstruaciones = [];

    // Consultar las menstruaciones positivas de cada paciente durante los últimos 7 días (sin contar el día actual)
    $menstruaciones = Menstruaciones::find()
        ->select(['idPacientes', 'fecha'])
        ->where(['positivo' => 1])
        ->andWhere(['>=', 'fecha', date('Y-m-d', strtotime('-8 days'))]) // Restar 8 días para excluir el día actual
        ->andWhere(['<', 'fecha', $fechaActual]) // Usar '<' en lugar de '<=' para excluir el día actual
        ->orderBy(['idPacientes' => SORT_ASC, 'fecha' => SORT_DESC])
        ->all();

    // Array asociativo para mantener el conteo de días consecutivos con menstruaciones positivas para cada paciente
    $diasConMenstruaciones = [];

    foreach ($menstruaciones as $menstruacion) {
        if (!isset($diasConMenstruaciones[$menstruacion->idPacientes])) {
            $diasConMenstruaciones[$menstruacion->idPacientes] = 1;
        } else {
            $diasConMenstruaciones[$menstruacion->idPacientes]++;
        }

        // Verificar si el paciente ha tenido menstruación positiva durante más de 7 días consecutivos
        if ($diasConMenstruaciones[$menstruacion->idPacientes] > 7) {
            // Obtener el nombre del paciente y agregarlo a las alertas de menstruaciones
            $paciente = Pacientes::findOne($menstruacion->idPacientes);
            $alertasMenstruaciones[] = ['nombre' => $paciente->nombreCompleto];
        }
    }

    // Renderizar la vista 'alertasMenstruaciones' y pasar los datos a la vista
    return $this->render('alertasMenstruaciones', [
        'alertasMenstruaciones' => $alertasMenstruaciones,
    ]);
}

  public function actionTodasrevisiones()
    {
      // Obtener todas las revisiones de manos y pies ordenadas por la fecha de revisión de manos de más reciente a menos reciente
        $dataProvider = new ActiveDataProvider([
            'query' => Revisiones::find()->orderBy(['fechaManos' => SORT_DESC]),
            'pagination' => [
            'pageSize' => 10, // Número de registros por página
        ],
        ]);



        return $this->render('todasRevisiones', [
            'dataProvider' => $dataProvider,
            
        ]);
    }
    
    
public function actionTodoslospartes()
{
     // Obtener todos los partes agrupados por paciente
    $dataProviderPartesPorPaciente = new ActiveDataProvider([
        'query' => Partes::find()
            ->select(['partes.*'])
            ->innerJoin('rellenados', 'rellenados.idPartes = partes.id')
            ->innerJoin('pacientes', 'pacientes.id = rellenados.idPacientes')
            ->orderBy('pacientes.nombreCompleto'),
    ]);

    // Renderizar la vista y pasar los DataProviders
    return $this->render('todoslospartes', [
        'dataProviderPartesPorPaciente' => $dataProviderPartesPorPaciente,
    ]);
}


    
     public function actionAlertaspeso()
{
    
    // Renderizar la vista y pasar los datos necesarios
    return $this->render('alertaspeso', [
        
    ]);
}

    
public function actionCamas()
{
    $habitaciones = [];
    for ($i = 1; $i <= 10; $i++) {
        $habitacion = \app\models\Habitaciones::findOne(['numeroHabitacion' => $i]);
        $estadoPuerta = $habitacion ? ($habitacion->estadoPuerta ? 'Cerrada' : 'Abierta') : 'Desconocido';
        $habitaciones[] = [
            'numero' => $i,
            'estadoPuerta' => $estadoPuerta,
        ];
    }

    return $this->render('camas', [
        'habitaciones' => $habitaciones,
    ]);
}


public function actionHabitacion1()
{
    // Obtener los pacientes de la habitación 1
    $pacientes = Pacientes::find()
        ->joinWith('camas')
        ->where(['camas.numHabitacion' => 1]) // Cambia el número de habitación según sea necesario
        ->all();

    // Agrupar a los pacientes por la letra de la cama
    $pacientesPorLetraCama = [];
    foreach ($pacientes as $paciente) {
        $letraCama = $paciente->camas->letraCama;
        if (!isset($pacientesPorLetraCama[$letraCama])) {
            $pacientesPorLetraCama[$letraCama] = [];
        }
        $pacientesPorLetraCama[$letraCama][] = $paciente;
    }

    // Ordenar a los pacientes por letra de cama
    ksort($pacientesPorLetraCama);

    return $this->render('habitacion1', [
        'pacientesPorLetraCama' => $pacientesPorLetraCama,
    ]);
}

public function actionHabitacion2()
{
    // Obtener los pacientes de la habitación 1
    $pacientes = Pacientes::find()
        ->joinWith('camas')
        ->where(['camas.numHabitacion' => 2]) // Cambia el número de habitación según sea necesario
        ->all();

    // Agrupar a los pacientes por la letra de la cama
    $pacientesPorLetraCama = [];
    foreach ($pacientes as $paciente) {
        $letraCama = $paciente->camas->letraCama;
        if (!isset($pacientesPorLetraCama[$letraCama])) {
            $pacientesPorLetraCama[$letraCama] = [];
        }
        $pacientesPorLetraCama[$letraCama][] = $paciente;
    }

    // Ordenar a los pacientes por letra de cama
    ksort($pacientesPorLetraCama);

    return $this->render('habitacion2', [
        'pacientesPorLetraCama' => $pacientesPorLetraCama,
    ]);
}

public function actionHabitacion3()
{
    // Obtener los pacientes de la habitación 1
    $pacientes = Pacientes::find()
        ->joinWith('camas')
        ->where(['camas.numHabitacion' => 3]) // Cambia el número de habitación según sea necesario
        ->all();

    // Agrupar a los pacientes por la letra de la cama
    $pacientesPorLetraCama = [];
    foreach ($pacientes as $paciente) {
        $letraCama = $paciente->camas->letraCama;
        if (!isset($pacientesPorLetraCama[$letraCama])) {
            $pacientesPorLetraCama[$letraCama] = [];
        }
        $pacientesPorLetraCama[$letraCama][] = $paciente;
    }

    // Ordenar a los pacientes por letra de cama
    ksort($pacientesPorLetraCama);

    return $this->render('habitacion3', [
        'pacientesPorLetraCama' => $pacientesPorLetraCama,
    ]);
}

public function actionHabitacion4()
{
    // Obtener los pacientes de la habitación 1
    $pacientes = Pacientes::find()
        ->joinWith('camas')
        ->where(['camas.numHabitacion' => 4]) // Cambia el número de habitación según sea necesario
        ->all();

    // Agrupar a los pacientes por la letra de la cama
    $pacientesPorLetraCama = [];
    foreach ($pacientes as $paciente) {
        $letraCama = $paciente->camas->letraCama;
        if (!isset($pacientesPorLetraCama[$letraCama])) {
            $pacientesPorLetraCama[$letraCama] = [];
        }
        $pacientesPorLetraCama[$letraCama][] = $paciente;
    }

    // Ordenar a los pacientes por letra de cama
    ksort($pacientesPorLetraCama);

    return $this->render('habitacion4', [
        'pacientesPorLetraCama' => $pacientesPorLetraCama,
    ]);
}

public function actionHabitacion5()
{
    // Obtener los pacientes de la habitación 1
    $pacientes = Pacientes::find()
        ->joinWith('camas')
        ->where(['camas.numHabitacion' => 5]) // Cambia el número de habitación según sea necesario
        ->all();

    // Agrupar a los pacientes por la letra de la cama
    $pacientesPorLetraCama = [];
    foreach ($pacientes as $paciente) {
        $letraCama = $paciente->camas->letraCama;
        if (!isset($pacientesPorLetraCama[$letraCama])) {
            $pacientesPorLetraCama[$letraCama] = [];
        }
        $pacientesPorLetraCama[$letraCama][] = $paciente;
    }

    // Ordenar a los pacientes por letra de cama
    ksort($pacientesPorLetraCama);

    return $this->render('habitacion5', [
        'pacientesPorLetraCama' => $pacientesPorLetraCama,
    ]);
}

public function actionHabitacion6()
{
    // Obtener los pacientes de la habitación 1
    $pacientes = Pacientes::find()
        ->joinWith('camas')
        ->where(['camas.numHabitacion' => 6]) // Cambia el número de habitación según sea necesario
        ->all();

    // Agrupar a los pacientes por la letra de la cama
    $pacientesPorLetraCama = [];
    foreach ($pacientes as $paciente) {
        $letraCama = $paciente->camas->letraCama;
        if (!isset($pacientesPorLetraCama[$letraCama])) {
            $pacientesPorLetraCama[$letraCama] = [];
        }
        $pacientesPorLetraCama[$letraCama][] = $paciente;
    }

    // Ordenar a los pacientes por letra de cama
    ksort($pacientesPorLetraCama);

    return $this->render('habitacion6', [
        'pacientesPorLetraCama' => $pacientesPorLetraCama,
    ]);
}

public function actionHabitacion7()
{
    // Obtener los pacientes de la habitación 1
    $pacientes = Pacientes::find()
        ->joinWith('camas')
        ->where(['camas.numHabitacion' => 7]) // Cambia el número de habitación según sea necesario
        ->all();

    // Agrupar a los pacientes por la letra de la cama
    $pacientesPorLetraCama = [];
    foreach ($pacientes as $paciente) {
        $letraCama = $paciente->camas->letraCama;
        if (!isset($pacientesPorLetraCama[$letraCama])) {
            $pacientesPorLetraCama[$letraCama] = [];
        }
        $pacientesPorLetraCama[$letraCama][] = $paciente;
    }

    // Ordenar a los pacientes por letra de cama
    ksort($pacientesPorLetraCama);

    return $this->render('habitacion7', [
        'pacientesPorLetraCama' => $pacientesPorLetraCama,
    ]);
}

public function actionHabitacion8()
{
    // Obtener los pacientes de la habitación 1
    $pacientes = Pacientes::find()
        ->joinWith('camas')
        ->where(['camas.numHabitacion' => 8]) // Cambia el número de habitación según sea necesario
        ->all();

    // Agrupar a los pacientes por la letra de la cama
    $pacientesPorLetraCama = [];
    foreach ($pacientes as $paciente) {
        $letraCama = $paciente->camas->letraCama;
        if (!isset($pacientesPorLetraCama[$letraCama])) {
            $pacientesPorLetraCama[$letraCama] = [];
        }
        $pacientesPorLetraCama[$letraCama][] = $paciente;
    }

    // Ordenar a los pacientes por letra de cama
    ksort($pacientesPorLetraCama);

    return $this->render('habitacion8', [
        'pacientesPorLetraCama' => $pacientesPorLetraCama,
    ]);
}

public function actionHabitacion9()
{
    // Obtener los pacientes de la habitación 1
    $pacientes = Pacientes::find()
        ->joinWith('camas')
        ->where(['camas.numHabitacion' => 9]) // Cambia el número de habitación según sea necesario
        ->all();

    // Agrupar a los pacientes por la letra de la cama
    $pacientesPorLetraCama = [];
    foreach ($pacientes as $paciente) {
        $letraCama = $paciente->camas->letraCama;
        if (!isset($pacientesPorLetraCama[$letraCama])) {
            $pacientesPorLetraCama[$letraCama] = [];
        }
        $pacientesPorLetraCama[$letraCama][] = $paciente;
    }

    // Ordenar a los pacientes por letra de cama
    ksort($pacientesPorLetraCama);

    return $this->render('habitacion9', [
        'pacientesPorLetraCama' => $pacientesPorLetraCama,
    ]);
}

public function actionHabitacion10()
{
    // Obtener los pacientes de la habitación 1
    $pacientes = Pacientes::find()
        ->joinWith('camas')
        ->where(['camas.numHabitacion' => 10]) // Cambia el número de habitación según sea necesario
        ->all();

    // Agrupar a los pacientes por la letra de la cama
    $pacientesPorLetraCama = [];
    foreach ($pacientes as $paciente) {
        $letraCama = $paciente->camas->letraCama;
        if (!isset($pacientesPorLetraCama[$letraCama])) {
            $pacientesPorLetraCama[$letraCama] = [];
        }
        $pacientesPorLetraCama[$letraCama][] = $paciente;
    }

    // Ordenar a los pacientes por letra de cama
    ksort($pacientesPorLetraCama);

    return $this->render('habitacion10', [
        'pacientesPorLetraCama' => $pacientesPorLetraCama,
    ]);
}

}





    

