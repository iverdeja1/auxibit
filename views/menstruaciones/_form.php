<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Menstruaciones $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="menstruaciones-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'positivo')->textInput() ?>

    <?= $form->field($model, 'fecha')->textInput() ?>

    <?= $form->field($model, 'idPacientes')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
