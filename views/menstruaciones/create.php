<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Menstruaciones $model */

$this->title = 'Create Menstruaciones';
$this->params['breadcrumbs'][] = ['label' => 'Menstruaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menstruaciones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
