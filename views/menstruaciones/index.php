<?php

use app\models\Menstruaciones;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Menstruaciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menstruaciones-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Menstruaciones', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
//
            'id',
//                'paciente.nombreCompleto',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Menstruaciones $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
            
            'fecha',
            'positivo',
         
        ],
    ]); ?>


</div>
