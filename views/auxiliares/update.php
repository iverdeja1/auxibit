<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Auxiliares $model */

$this->title = 'Update Auxiliares: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Auxiliares', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="auxiliares-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
