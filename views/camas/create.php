<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Camas $model */

$this->title = 'Create Camas';
$this->params['breadcrumbs'][] = ['label' => 'Camas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="camas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
