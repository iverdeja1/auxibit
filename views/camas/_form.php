<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Camas $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="camas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'letraCama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'numHabitacion')->textInput() ?>

    <?= $form->field($model, 'ocupacion')->textInput() ?>

    <?= $form->field($model, 'idPacientes')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
