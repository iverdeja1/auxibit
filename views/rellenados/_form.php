<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Rellenados $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="rellenados-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idPartes')->textInput() ?>

    <?= $form->field($model, 'idPacientes')->textInput() ?>

    <?= $form->field($model, 'idAuxiliares')->textInput() ?>

    <?= $form->field($model, 'fecha')->textInput() ?>

    <?= $form->field($model, 'turno')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
