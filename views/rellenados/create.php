<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Rellenados $model */

$this->title = 'Create Rellenados';
$this->params['breadcrumbs'][] = ['label' => 'Rellenados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rellenados-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
