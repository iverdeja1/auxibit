 <?php

use app\models\Partes;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Partes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Partes', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
//
//            'id',
            [ 
 
    'attribute' => 'id', 
 
    'visible' => false 
 
],
            'descripcion:ntext',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Partes $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>