<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Partes $model */

$this->title = 'Update Partes: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Partes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="partes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
