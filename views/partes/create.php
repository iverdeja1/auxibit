<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Partes $model */

$this->title = 'Create Partes';
$this->params['breadcrumbs'][] = ['label' => 'Partes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
