<?php

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="author" content="Irene Verdeja Díaz">
    <meta name="description" content="Aplicación para el cuidado de pacientes">
    <meta name="keywords" content="HTML, CSS, JavaScript, React">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    
    <title>AuxiBit | Aplicación web para el cuidado de pacientes</title>
    <?php $this->head() ?>
    
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?= Yii::getAlias('@web') ?>/auxibitfavicon.ico" type="image/x-icon">

    <!-- Inclusión de jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    
    <!-- Importación de los estilos de Bootstrap Icons -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
  
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<header>
    <?php
    NavBar::begin([
        'brandLabel' => Html::img('@web/images/LogoGandara.png', ['alt'=>Yii::$app->name]),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-expand-md navbar-dark bg-dark fixed-top', // Quité la clase 'navbar-custom'
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
         'items' => [
            [
                'label' => '<i class="bi bi-hospital"></i> INICIO',
                'url' => ['/site/index'],
                'encode' => false,
                'options' => ['class' => 'nav-item'],
            ],
            [
                'label' => '<i class="bi bi-person-vcard"></i> PACIENTES',
                'url' => ['/site/mostrarpacientes'],
                'encode' => false,
                'options' => ['class' => 'nav-item'],
            ],
            [
                'label' => '<i class="bi bi-clipboard-pulse"></i> REGISTROS',
                'url' => ['/site/registros'],
                'encode' => false,
                'options' => ['class' => 'nav-item'],
            ],
            [
                'label' => '<i class="bi bi-map"></i> MAPA',
                'url' => ['/site/camas'],
                'encode' => false,
                'options' => ['class' => 'nav-item'],
            ],
            [
                'label' => '<i class="bi bi-exclamation-triangle"></i></i> INCIDENCIAS',
                'url' => ['/site/mostraralertas'],
                'encode' => false,
                'options' => ['class' => 'nav-item'],
            ],
            [
                'label' => '<i class="bi bi-file-earmark-medical"></i> PROTOCOLOS',
                'url' => ['/site/protocolos'],
                'encode' => false,
                'options' => ['class' => 'nav-item'],
            ],
        Yii::$app->user->isGuest ? (
                        ['label' => 'LOGIN', 'url' => ['/site/login']]
                    ) : (
                        [
                            'label' => 'LOGOUT (' . Yii::$app->user->identity->username . ')',
                            'url' => ['/site/logout'],
                            'linkOptions' => ['data-method' => 'post'],
                        ]
                    )
                ],
            ]);
    NavBar::end();
    ?>
</header>

<main role="main" class="flex-shrink-0">
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</main>

<footer class="footer mt-auto py-3 text-muted">
    <div class="container">
        <p class="float-left">&copy; AuxiBit <?= date('Y') ?></p>
    </div>
</footer>
<script src="script.js"></script> <!-- Enlaza el archivo JavaScript externo -->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
