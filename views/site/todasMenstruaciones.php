<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;

$this->title = 'Todas las menstruaciones';
$this->params['breadcrumbs'][] = ['label' => 'Registros', 'url' => ['/site/registros']];
$this->params['breadcrumbs'][] = $this->title;

// Registra tu archivo CSS personalizado
$this->registerCssFile('@web/css/custom-styles.css');

// Incluye tu script JavaScript
$script = <<<JS
document.addEventListener('DOMContentLoaded', function() {
    // Encuentra todos los enlaces dentro del contenedor de paginación y les aplica la clase btn-custom
    document.querySelectorAll('.pagination a').forEach(function(element) {
        element.classList.add('btn-custom');
    });
});
JS;

// Registra el script en la vista
$this->registerJs($script, \yii\web\View::POS_READY);
?>

<div class="card mb-4"> <!-- Agregado mb-4 para añadir margen inferior -->
    <div class="card-body text-center"> <!-- Centrado del texto -->
        <h4 class="card-title">Historial de menstruaciones de todos los pacientes</h4>
    </div>
</div>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'attribute' => 'idPacientes',
            'label' => 'Paciente',
            'value' => function ($model) {
                return $model->idPacientes0->nombreCompleto;
            },
        ],
        [
            'attribute' => 'positivo',
            'label' => 'Presenta sangrado',
            'value' => function ($model) {
                return $model->positivo ? 'Sí' : 'No';
            },
        ],
        [
            'attribute' => 'fecha',
            'label' => 'Fecha',
        ],
    ],
    'layout' => "{items}", // Elimina la paginación y el resumen de la tabla
    'options' => [
        'class' => 'table table-bordered table-striped', // Añade estilos de bootstrap a la tabla
    ],
]) ?>

<div class="row">
    <div class="col-sm-12 text-center"> <!-- Centra la paginación -->
        <!-- Paginación -->
        <?= LinkPager::widget([
            'pagination' => $dataProvider->pagination,
            'nextPageLabel' => 'Siguiente',
            'prevPageLabel' => 'Anterior',
            'maxButtonCount' => 5, 
            'options' => ['class' => 'pagination justify-content-center pagination-container'], 
            'linkOptions' => ['class' => 'page-link'], 
            'activePageCssClass' => 'active', 
            'pageCssClass' => 'page-item', 
            'hideOnSinglePage' => true,
        ]); ?>
    </div>
</div>
