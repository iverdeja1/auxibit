<?php
use yii\helpers\Html;

$this->title = 'Alertas de Enema';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card mb-4"> <!-- Agregado mb-4 para añadir margen inferior -->
    <div class="card-body text-center"> <!-- Centrado del texto -->
        <h4 class="card-title">Incidencias de enemas</h4>
    </div>
</div>

<?php if (!empty($alertasEnema)) : ?>
<div class="alert alert-warning card" role="alert">
    <h4 class="alert-heading">Incidencia Enema</h4>
    <p>Los siguientes pacientes no han realizado deposición durante 4 días consecutivos:</p>
    <ul>
        <?php foreach ($alertasEnema as $paciente) : ?>
            <li><?= Html::encode($paciente) ?></li>
        <?php endforeach; ?>
    </ul>
</div>
<?php else : ?>
    <div class="alert alert-info card" role="alert">
        No hay incidencias de enema en este momento.
    </div>
<?php endif; ?>