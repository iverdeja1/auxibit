<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\bootstrap\BootstrapAsset;
use yii\helpers\Url;

$this->title = 'My Yii Application';

?>

<div class="container mt-4">
    <div class="row justify-content-center">
        <div class="col-md-6 text-center">
            <?= Html::img(Yii::getAlias('@web') . '/images/AuxiBitTexto1.png', ['class' => 'img-fluid mb-3']) ?> <!-- Añadido mb-5 para margen inferior -->
        </div>
    </div>
</div>

<div class="card">
    <div class="card-body text-center"> <!-- Centrado del texto -->
        <h4 class="card-title">Panel de tareas</h4>
    </div>
</div>

<div class="container mt-4">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-hover">
                <tbody>
                    <tr>
                        <td>
                            <h5>Censo de los pacientes en hospitalización completa.</h5>
                        </td>
                        <td class="d-flex justify-content-center"> <!-- Agregada la clase d-flex y justify-content-center -->
                            <?= Html::a('<div class="button-content"><h5 class="m-0"><i class="bi bi-person-vcard"></i> ' . strtoupper('PACIENTES') . '</h5></div>', ['site/mostrarpacientes'], ['class' => 'btn btn-primary btn-custom-size']) ?>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <h5>Registros clínicos de enfermería de los pacientes.</h5>
                        </td>
                           <td class="d-flex justify-content-center"> <!-- Agregada la clase d-flex y justify-content-center -->
                           <?= Html::a('<div class="button-content"><h5 class="m-0"><i class="bi bi-clipboard-pulse"></i> ' . strtoupper('REGISTROS') . '</h5></div>', ['site/registros'], ['class' => 'btn btn-primary btn-custom-size btn-sm']) ?>
                        </td>           
                    </tr>
                    <tr>
                        <td>
                            <h5>Mapa de la planta donde residen los pacientes.</h5>
                        </td>
                          <td class="d-flex justify-content-center"> <!-- Agregada la clase d-flex y justify-content-center -->
                           <?= Html::a('<div class="button-content"><h5 class="m-0"><i class="bi bi-map"></i> ' . strtoupper('MAPA') . '</h5></div>', ['site/camas'], ['class' => 'btn btn-primary btn-custom-size btn-sm']) ?>
                        </td>                      
                    </tr>
                    <tr>
                        <td>
                            <h5>Incidencias clínicas.</h5>
                        </td>
                         <td class="d-flex justify-content-center"> <!-- Agregada la clase d-flex y justify-content-center -->
                            <?= Html::a('<div class="button-content"><h5 class="m-0"><i class="bi bi-exclamation-triangle"></i> ' . strtoupper('INCIDENCIAS') . '</h5></div>', ['site/mostraralertas'], ['class' => 'btn btn-primary btn-custom-size btn-sm']) ?>
                        </td>    
                    </tr>
                    <tr>
                        <td>
                            <h5>Protocolos de actuación clínica en pacientes.</h5>
                        </td>
                          <td class="d-flex justify-content-center"> <!-- Agregada la clase d-flex y justify-content-center -->
                            <?= Html::a('<div class="button-content"><h5 class="m-0"><i class="bi bi-file-earmark-medical"></i> ' . strtoupper('PROTOCOLOS') . '</h5></div>', ['site/protocolos'], ['class' => 'btn btn-primary btn-custom-size btn-sm']) ?>
                        </td>  
                    </tr>
                    <tr>
                        <td>
                            <h5>Teléfonos de los Técnicos de Enfermería de la planta.</h5>
                        </td>
                            <td class="d-flex justify-content-center"> <!-- Agregada la clase d-flex y justify-content-center -->
                            <?= Html::a('<div class="button-content"><h5 class="m-0"><i class="bi bi-telephone"></i> ' . strtoupper('TELÉFONOS') . '</h5></div>', ['site/tecnicos'], ['class' => 'btn btn-primary btn-custom-size btn-sm']) ?>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>


<!-- Estilos CSS -->
<style>
    .table-hover tbody tr:hover {
        background-color: #e7e8e8; /* Cambia el color de fondo al pasar el ratón */
    }
</style>
