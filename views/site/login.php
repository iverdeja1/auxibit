<?php

/** @var yii\web\View $this */
/** @var yii\bootstrap4\ActiveForm $form */
/** @var app\models\LoginForm $model */

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;

?>
<div class="site-login" style="margin-top: 50px; text-align: center;">
     <!-- Añadir la imagen del logo -->
    <div class="logo">
        <!-- Prueba con una ruta absoluta si la relativa no funciona -->
        <img src="<?= Yii::getAlias('@web') ?>/images/AuxiBitTexto1.png" alt="Logo" style="max-width: 500px; margin-bottom: 20px;">
    </div>

    <div class="row justify-content-center">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'layout' => 'horizontal',
                'fieldConfig' => [
                    'template' => "{label}\n{input}\n{error}",
                    'labelOptions' => ['class' => 'col-lg-4 col-form-label'],
                    'inputOptions' => ['class' => 'form-control'],
                    'errorOptions' => ['class' => 'invalid-feedback'],
                ],
            ]); ?>

            <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label('Usuario') ?>


            <?= $form->field($model, 'password')->passwordInput()->label('Contraseña') ?>

            <div class="form-group">
                <div class="col-lg-10 offset-lg-1">
                    <?= Html::submitButton('ENTRAR', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
