<?php
use yii\helpers\Html;

$this->title = 'Camas';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Muestra el mapa de la cama -->
<div class="row justify-content-center">
    <div class="col-md-10">
        <div class="card text-center">
            <div class="card-body">
                <h3 class="habitacion" data-numero="1">Mapa de las camas de la unidad</h3>
                <img id="mapaCama1" class="img-fluid mt-3" src="<?= Yii::$app->request->baseUrl ?>/images/MapaCama0.png" alt="Mapa de la Cama 0" style="max-width: 50%; height: auto;">
            </div>
        </div>  
    </div>
</div>

<div class="row justify-content-center">
    <div class="col-md-10">
        <table class="table table-hover"> 
            <thead>
                <tr>
                    <th>Habitación</th>
                    <th>Puerta</th>
                    <th>Detalles</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($habitaciones as $habitacion): ?>
                    <tr>
                        <td>Habitación <?= $habitacion['numero'] ?></td>
                        <td><?= $habitacion['estadoPuerta'] ?></td>
                        <td><?= Html::a('Ver más', ['habitacion' . $habitacion['numero']], ['class' => 'btn btn-primary']) ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<div class="row justify-content-center">
    <div class="col-md-10">
        <!-- Contenido aquí -->
        <div class="card text-center">
            <div class="card-body">
                <p>Las puertas de las habitaciones deben quedar abiertas o cerradas al acostar según se indique en cada habitación.</p>
            </div>
        </div>
    </div>
</div>
