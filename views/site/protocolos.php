<?php

use yii\helpers\Html;

$this->title = 'Protocolos';
$this->params['breadcrumbs'][] = $this->title;
?>

  <div class="card">
        <div class="card-body text-center"> <!-- Centrado del texto -->
            <h4 class="card-title">Protocolos</h4>
        </div>
    </div>


<div class="container mt-4">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-hover"> <!-- Añadido table-hover para efecto hover -->
                <tbody>
                    <tr>
                        <td>
                            <h5>Protocolo de reanimación cardio pulmonar</h5>
                        </td>
                        <td>
                            <?= Html::a('Ver más', ['site/verprotocolorcp'], ['class' => 'btn btn-primary', 'data-title' => 'RCP', 'target' => '_blank']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h5>Protocolo de actuación frente a un atragantamiento</h5>
                        </td>
                        <td>
                            <?= Html::a('Ver más', ['site/verprotocoloatragantamiento'], ['class' => 'btn btn-primary', 'data-title' => 'Atragantamiento', 'target' => '_blank']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h5>Protocolo para pacientes hospitalizados con necesidad de contención</h5>
                        </td>
                        <td>
                            <?= Html::a('Ver más', ['site/verprotocolocontencion'], ['class' => 'btn btn-primary', 'data-title' => 'Contención', 'target' => '_blank']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h5>Protocolos de movilización del paciente</h5>
                        </td>
                        <td>
                            <?= Html::a('Ver más', ['site/verprotocolomovilizacion'], ['class' => 'btn btn-primary', 'data-title' => 'Movilización', 'target' => '_blank']) ?>
                        </td>
                    </tr>              
                </tbody>
            </table>
        </div>
    </div>
</div>





