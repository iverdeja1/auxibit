<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\LinkPager;

$this->title = 'Pacientes';
$this->params['breadcrumbs'][] = $this->title;

// Registra tu archivo CSS personalizado
$this->registerCssFile('@web/css/custom-styles.css');

// Incluye tu script JavaScript
$script = <<<JS
document.addEventListener('DOMContentLoaded', function() {
    // Encuentra todos los enlaces dentro del contenedor de paginación y les aplica la clase btn-custom
    document.querySelectorAll('.pagination a').forEach(function(element) {
        element.classList.add('btn-custom');
    });
});
JS;

// Registra el script en la vista
$this->registerJs($script, \yii\web\View::POS_READY);
?>
<div class="card">
        <div class="card-body text-center"> <!-- Centrado del texto -->
            <h4 class="card-title">Censo de pacientes en hospitalzación completa</h4>
        </div>
    </div>
<div class="container mt-5">
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Número Historia</th>
                    <th>Edad</th>                 
                    <th>Habitación</th>
                    <th>Cama</th>
                    <th>Detalles</th>
                </tr>
            </thead>
            <tbody>
                <?= ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemView' => '_pacientes2', // Se actualizará para generar filas de tabla
                    'layout' => "{items}",
                    'options' => ['tag' => false], // No envuelve el contenedor principal
                    'itemOptions' => ['tag' => false], // No envuelve cada ítem en una etiqueta adicional
                ]); ?>
            </tbody>
        </table>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 text-center"> <!-- Centra la paginación -->
        <!-- Paginación -->
        <?= LinkPager::widget([
            'pagination' => $dataProvider->pagination,
            'nextPageLabel' => 'Siguiente',
            'prevPageLabel' => 'Anterior',
            'maxButtonCount' => 5, 
            'options' => ['class' => 'pagination justify-content-center pagination-container'], 
            'linkOptions' => ['class' => 'page-link'], 
            'activePageCssClass' => 'active', 
            'pageCssClass' => 'page-item', 
            'hideOnSinglePage' => true, 
        ]); ?>
    </div>
</div>
