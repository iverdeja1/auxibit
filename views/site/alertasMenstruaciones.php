<?php
use yii\helpers\Html;

$this->title = 'Alertas de Menstruaciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card mb-4"> <!-- Agregado mb-4 para añadir margen inferior -->
    <div class="card-body text-center"> <!-- Centrado del texto -->
        <h4 class="card-title">Incidencias de menstruaciones</h4>
    </div>
</div>

<?php if (!empty($alertasMenstruaciones)) : ?>
    <div class="alert alert-warning card" role="alert">
        <h4 class="alert-heading">Avisar a la enfermera</h4>
        <p>Los siguientes pacientes han presentado sangrado durante más de 7 días consecutivos:</p>
        <ul>
            <?php foreach ($alertasMenstruaciones as $paciente) : ?>
                <li><?= Html::encode($paciente['nombre']) ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php else : ?>
    <div class="alert alert-info card" role="alert">
        No hay incidencias de menstruaciones en este momento.
    </div>
<?php endif; ?>
