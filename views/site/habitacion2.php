<?php
use yii\helpers\Html;

$this->title = 'Habitación 1';
$this->params['breadcrumbs'][] = ['label' => 'Camas', 'url' => ['/site/camas']];
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Muestra el mapa de la cama -->
<div class="row justify-content-center">
    <div class="col-md-10">
        <div class="card text-center">
            <div class="card-body">
                <h3 class="habitacion" data-numero="2">Has entrado en la habitación 2</h3>
                <img id="mapaCama2" class="img-fluid mt-3" src="<?= Yii::$app->request->baseUrl ?>/images/MapaCama2.png" alt="Mapa de la Cama 2" style="max-width: 50%; height: auto;">
            </div>
        </div>  
    </div>
</div>

<!-- Muestra la información de los pacientes -->
<div class="row justify-content-center mt-3">
    <div class="col-md-10">
        <table class="table table-bordered table-striped mt-3">
            <thead>
                <tr>
                    <th>Nombre del Paciente</th>
                    <th>Letra de la Cama</th>
                    <th>Detalles</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($pacientesPorLetraCama as $letraCama => $pacientesLetraCama) : ?>
                    <?php foreach ($pacientesLetraCama as $paciente) : ?>
                        <tr>
                            <td><?= Html::encode($paciente->nombreCompleto) ?></td>
                            <td><?= Html::encode($paciente->camas->letraCama) ?></td>
                            <td><?= Html::a('Ver más', ['/site/registrospartes', 'pacienteId' => $paciente->id], ['class' => 'btn btn-primary']) ?></td>
                        </tr>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
