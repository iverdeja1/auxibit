<?php
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Historial de registros de ' . $model->nombreCompleto;
$this->params['breadcrumbs'][] = ['label' => 'Pacientes', 'url' => ['/site/mostrarpacientes']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="card mb-4"> <!-- Agregado mb-4 para añadir margen inferior -->
    <div class="card-body text-center"> <!-- Centrado del texto -->
        <h4 class="card-title">Historial de partes</h4>
    </div>
</div>
<div>
    <label class="control-label">Nombre del Paciente</label>
    <h3><?= Html::encode($model->nombreCompleto) ?></h3>
</div>

<br><br>

<div id="partes-table">
    <?= GridView::widget([
        'dataProvider' => $dataProviderPartes,
        'columns' => [
            'descripcion:html',
        ],
        'pager' => [
            'options' => ['class' => 'pagination'],
            'hideOnSinglePage' => true,
            'nextPageLabel' => 'Siguiente',
            'prevPageLabel' => 'Anterior',
        ],
        'summary' => false,
    ]); ?>
</div>
