<?php
use yii\helpers\Html;

/** @var \app\models\Pacientes $model */
?>
    

<tr>
    <td><?= Html::encode($model->nombreCompleto) ?></td>
    <td><?= Html::encode($model->numeroHistoria) ?></td>
    <td><?= Html::encode($model->edad) ?></td>
    <td><?= Html::encode($model->camas->numHabitacion) ?></td> <!-- Accede al número de habitación a través de la relación Camas->Habitaciones -->
    <td><?= Html::encode($model->camas->letraCama) ?></td>
    <td>
        <?= Html::a('Ver más', ['site/registrospartes', 'pacienteId' => $model->id], ['class' => 'btn btn-primary']) ?>
    </td>
</tr>
