<?php
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

$this->title = 'Registros de ' . $model->nombreCompleto;
$this->params['breadcrumbs'][] = ['label' => 'Pacientes', 'url' => ['/site/mostrarpacientes']];
$this->params['breadcrumbs'][] = $this->title;

?>


    <div class="card">
        <div class="card-body text-center"> <!-- Centrado del texto -->
            <h4 class="card-title"><?= Html::encode($model->nombreCompleto) ?></h4>
        </div>
    </div>

<h2>Deposiciones</h2>
<div class="d-flex justify-content-between align-items-center">
    <?= Html::a('Crear nuevo registro', ['site/creardeposicion', 'pacienteId' => $pacienteId], ['class' => 'btn btn-primary btn-md mr-2']) ?>
    <?= Html::a('Historial de deposiciones', ['site/historialdeposiciones', 'pacienteId' => $pacienteId], ['class' => 'btn btn-primary btn-md']) ?>
</div>
<br><br>
<div id="deposiciones-table">
    <?= GridView::widget([
        'dataProvider' => $dataProviderDeposiciones,
        'columns' => [
            'turno',
            'fecha',
               [
                'attribute' => 'positivo',
                'value' => function ($model) {
                    return $model->positivo ? 'Sí' : 'No';
        },
    ],
        ],
        'pager' => [
            'options' => ['class' => 'pagination'],
            'hideOnSinglePage' => true,
            'nextPageLabel' => 'Siguiente',
            'prevPageLabel' => 'Anterior',
        ],
        'summary' => false,
    ]); ?>
</div>
<br><br>
<?php if ($model->sexo === 'Femenino'): ?>
    <!-- Solo se muestra si el sexo del paciente es Femenino -->
    <h2>Menstruaciones</h2>
    <div class="d-flex justify-content-between align-items-center">
        <?= Html::a('Crear nuevo registro', ['site/crearmenstruacion', 'pacienteId' => $pacienteId], ['class' => 'btn btn-primary btn-md mr-2']) ?>
        <?= Html::a('Historial de menstruaciones', ['site/historialmenstruaciones', 'pacienteId' => $pacienteId], ['class' => 'btn btn-primary btn-md']) ?>
    </div>
    <br><br>
    <div id="menstruaciones-table">
        <?= GridView::widget([
            'dataProvider' => $dataProviderMenstruaciones,
            'columns' => [
                   [
                    'attribute' => 'positivo',
                    'value' => function ($model) {
                        return $model->positivo ? 'Sí' : 'No';
                    },
    ],
                'fecha',
            ],
            'pager' => [
                'options' => ['class' => 'pagination'],
                'hideOnSinglePage' => true,
                'nextPageLabel' => 'Siguiente',
                'prevPageLabel' => 'Anterior',
            ],
            'summary' => false,
        ]); ?>
    </div>
<?php endif; ?>
<br><br>
<h2>Revisiones</h2>
<div class="d-flex justify-content-between align-items-center">
    <?= Html::a('Crear nuevo registro', ['site/crearrevision', 'pacienteId' => $pacienteId], ['class' => 'btn btn-primary btn-md mr-2']) ?>
    <?= Html::a('Historial de revisiones', ['site/historialrevisiones', 'pacienteId' => $pacienteId], ['class' => 'btn btn-primary btn-md']) ?>
</div>
<br><br>
<div id="revisiones-table">
    <?= GridView::widget([
        'dataProvider' => $dataProviderRevisiones,
        'columns' => [
            'fechaManos',
               [
        'attribute' => 'manos',
        'value' => function ($model) {
            return $model->manos ? 'Sí' : 'No';
        },
    ],
            'fechaPies',
               [
        'attribute' => 'pies',
        'value' => function ($model) {
            return $model->pies ? 'Sí' : 'No';
        },
    ],
            'observacion:html',
        ],
        'pager' => [
            'options' => ['class' => 'pagination'],
            'hideOnSinglePage' => true,
            'nextPageLabel' => 'Siguiente',
            'prevPageLabel' => 'Anterior',
        ],
        'summary' => false,
    ]); ?>
</div>
<br><br>
<h2>Partes</h2>
<div class="d-flex justify-content-between align-items-center">
    <?= Html::a('Crear nuevo registro', ['site/crearparte', 'pacienteId' => $pacienteId], ['class' => 'btn btn-primary btn-md mr-2']) ?>
    <?= Html::a('Historial de partes', ['site/historialpartes', 'pacienteId' => $pacienteId], ['class' => 'btn btn-primary btn-md']) ?>
</div>
<br><br>
<div id="partes-table">
    <?= GridView::widget([
        'dataProvider' => $dataProviderPartes,
      'columns' => [
//        [
//            'attribute' => 'rellenados.fecha',
//            'label' => 'Fecha',
//        ],
//        [
//            'attribute' => 'rellenados.turno',
//            'label' => 'Turno',
//        ],
        'descripcion:html',
    ],
        'pager' => [
            'options' => ['class' => 'pagination'],
            'hideOnSinglePage' => true,
            'nextPageLabel' => 'Siguiente',
            'prevPageLabel' => 'Anterior',
        ],
        'summary' => false,
    ]); ?>
</div>

