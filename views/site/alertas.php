<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Alertas';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card mb-4"> <!-- Agregado mb-4 para añadir margen inferior -->
    <div class="card-body text-center"> <!-- Centrado del texto -->
        <h4 class="card-title">Incidencias del paciente</h4>
    </div>
</div>


<div class="container mt-4">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-hover"> <!-- Añadido table-hover para efecto hover -->
                <tbody>
                    <tr>
                        <td>
                            <h5>Incidencias de deposiciones para aplicar enemas</h5>
                        </td>
                        <td>
                            <?= Html::a('Ver más', ['site/alertasenema'], ['class' => 'btn btn-primary', 'data-title' => 'Enemas']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h5>Incidencias de menstruaciones para informar a la enfermera</h5>
                        </td>
                        <td>
                            <?= Html::a('Ver más', ['site/alertasmenstruaciones'], ['class' => 'btn btn-primary', 'data-title' => 'Alertas menstruaciones']) ?>
                        </td>
                    </tr>
<!--                    <tr>
                        <td>
                            <h5>Alertas IMC</h5>
                        </td>
                        <td>
                            <?= Html::a('Ver más', ['site/alertaspeso'], ['class' => 'btn btn-primary', 'data-title' => 'IMC']) ?>
                        </td>
                    </tr>                   -->
                </tbody>
            </table>
        </div>
    </div>
</div>


