<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Formulario de deposiciones';
$this->params['breadcrumbs'][] = ['label' => 'Pacientes', 'url' => ['/site/mostrarpacientes']];
$this->params['breadcrumbs'][] = $this->title;

// Aquí es donde inicializas el formulario
$form = ActiveForm::begin(); 
?>

<div class="card">
    <div class="card-body">
        <h3>Nuevo registro de deposiciones</h3>
    </div>
</div>

<?= $form->field($model, 'idPacientes')->hiddenInput()->label(false) ?>

<div>
    <label class="control-label">Nombre del Paciente</label>
    <h3><?= Html::encode($nombrePaciente) ?></h3>
</div>

<?= $form->field($model, 'fecha')->textInput(['value' => Yii::$app->formatter->asDate(date('Y-m-d'), 'php:d-m-Y'), 'readonly' => true]) ?>

<?= $form->field($model, 'turno')->textInput(['value' => $turnoPredeterminado, 'readonly' => true]) ?> <!-- Campo de solo lectura -->

<p>Realiza deposición:</p>
<?= $form->field($model, 'positivo')->radioList([
    '1' => 'Sí',
    '0' => 'No'
])->label(false) ?>
<div id="errorMensaje" style="display: none; color: red;">Debes seleccionar "Sí" o "No".</div>
<div class="form-group">
    <?= Html::button('Guardar', ['class' => 'btn btn-success', 'id' => 'guardarButton']) ?>
</div>

<?php ActiveForm::end(); ?>


<?php
$js = <<<JS
$('#guardarButton').click(function(event) {
    event.preventDefault(); // Evitar el envío predeterminado del formulario
    // Verificar si se ha seleccionado "Sí" o "No"
    var seleccionado = $('input[name="Deposiciones[positivo]"]:checked').val();
    if (seleccionado === '1' || seleccionado === '0') {
        $('#confirmacionModal').modal('show'); // Mostrar el modal de confirmación si se ha seleccionado "Sí" o "No"
        $('#errorMensaje').hide(); // Ocultar el mensaje de error si estaba mostrándose
    } else {
        // Si no se ha seleccionado "Sí" o "No", mostrar el mensaje de error
        $('#errorMensaje').show(); // Mostrar el mensaje de error
    }
});

// Evento de click para el botón de guardar dentro del modal de confirmación
$('#confirmarGuardar').click(function() {
    $('#confirmacionModal').modal('hide'); // Ocultar el modal de confirmación
    $('form').submit(); // Enviar el formulario
});
JS;
$this->registerJs($js);
?>


<!-- Modal de confirmación -->
<div class="modal fade" id="confirmacionModal" tabindex="-1" role="dialog" aria-labelledby="confirmacionModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-vertical-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmacionModalLabel">Confirmación</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Estás seguro de querer guardar? No podrás editar ni borrar después de guardar.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="confirmarGuardar">Guardar</button>
            </div>
        </div>
    </div>
</div>
