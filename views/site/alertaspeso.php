<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Alertas peso';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card mb-4"> <!-- Agregado mb-4 para añadir margen inferior -->
    <div class="card-body text-center"> <!-- Centrado del texto -->
        <h4 class="card-title">Incidencias del IMC</h4>
    </div>
</div>
