<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\LinkPager;

$this->title = 'Teléfonos';
$this->params['breadcrumbs'][] = $this->title;

$this->title = 'Técnicos de Enfermería';
//$this->params['breadcrumbs'][] = $this->title;

// Registra tu archivo CSS personalizado
$this->registerCssFile('@web/css/custom-styles.css');

// Incluye tu script JavaScript
$script = <<<JS
document.addEventListener('DOMContentLoaded', function() {
    // Encuentra todos los enlaces dentro del contenedor de paginación y les aplica la clase btn-custom
    document.querySelectorAll('.pagination a').forEach(function(element) {
        element.classList.add('btn-custom');
    });
});
JS;

// Registra el script en la vista
$this->registerJs($script, \yii\web\View::POS_READY);
?>

<div class="card">
    <div class="card-body text-center"> <!-- Centrado del texto -->
        <h4 class="card-title">Teléfonos de los Técnicos de Enfermería</h4>
    </div>
</div>

<div class="auxiliares-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'nombreAuxiliar', // Este es el nombre del auxiliar
            [
                'attribute' => 'telefono',
                'value' => function ($model) {
                    // Concatena todos los teléfonos asociados al auxiliar
                    return implode(', ', yii\helpers\ArrayHelper::map($model->telefonos, 'id', 'telefono'));
                },
            ],
        ],
        'layout' => "{items}", // Elimina la paginación y el resumen de la tabla
        'options' => [
            'class' => 'table table-bordered table-striped',
        ],
        'summary' => false,
    ]); ?>
</div>

<div class="row">
    <div class="col-sm-12 text-center"> <!-- Centra la paginación -->
        <!-- Paginación -->
        <?= LinkPager::widget([
            'pagination' => $dataProvider->pagination,
            'nextPageLabel' => 'Siguiente',
            'prevPageLabel' => 'Anterior',
            'maxButtonCount' => 5, 
            'options' => ['class' => 'pagination justify-content-center pagination-container'], 
            'linkOptions' => ['class' => 'page-link'], 
            'activePageCssClass' => 'active', 
            'pageCssClass' => 'page-item', 
            'hideOnSinglePage' => true,
        ]); ?>
    </div>
</div>
