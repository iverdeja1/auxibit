<?php

use yii\helpers\Html;

$this->title = 'Registros';
$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="card">
        <div class="card-body text-center"> <!-- Centrado del texto -->
            <h4 class="card-title">Registros clínicos</h4>
        </div>
    </div>


<div class="container mt-4"> <!-- Contenedor para la tabla -->
    <div class="row">
        <div class="col-md-12">
            <table class="table table-hover"> <!-- Añadido table-hover para efecto hover -->
                <tbody>
                    <tr>
                        <td>
                            <h5>Deposiciones</h5>
                        </td>
                        <td>
                            <?= Html::a('Ver más', ['site/todasdeposiciones'], ['class' => 'btn btn-primary', 'data-title' => 'Pacientes']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h5>Menstruaciones</h5>
                        </td>
                        <td>
                            <?= Html::a('Ver más', ['site/todasmenstruaciones'], ['class' => 'btn btn-primary', 'data-title' => 'Mapa de camas']) ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h5>Revisiones</h5>
                        </td>
                        <td>
                            <?= Html::a('Ver más', ['site/todasrevisiones'], ['class' => 'btn btn-primary', 'data-title' => 'Alertas']) ?>
                        </td>
                    </tr>  
                  
                </tbody>
            </table>
        </div>
    </div>
</div>
