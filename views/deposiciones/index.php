<?php

use app\models\Deposiciones;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Deposiciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="deposiciones-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Deposiciones', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
//
//            'id',
               'idPacientes',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Deposiciones $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
            'turno',
            'fecha',
            'positivo',
         
        ],
    ]); ?>


</div>
