<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Revisiones $model */

$this->title = 'Create Revisiones';
$this->params['breadcrumbs'][] = ['label' => 'Revisiones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="revisiones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
