document.addEventListener('DOMContentLoaded', function() {
  window.addEventListener('scroll', function() {
    var header = document.querySelector('.header');
    var footer = document.querySelector('.footer');
    
    if (window.scrollY > 0) {
      header.classList.add('header-shadow');
    } else {
      header.classList.remove('header-shadow');
    }
    
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
      footer.classList.add('footer-shadow');
    } else {
      footer.classList.remove('footer-shadow');
    }
  });
});
