// Script para expandir la barra lateral al cargar la página
document.addEventListener("DOMContentLoaded", function() {
    document.querySelector("#sidebar").classList.add("expand");
});

// Script para expandir/retraer la barra lateral
const hamBurger = document.querySelector(".toggle-btn");

hamBurger.addEventListener("click", function () {
    document.querySelector("#sidebar").classList.toggle("expand");
});
