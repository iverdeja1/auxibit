$(document).ready(function() {
    // Deshabilitar la edición de las celdas
    $("td.status-cell").prop('contenteditable', false);

    // Detectar clic en las celdas
    $("td.status-cell").click(function() {
        var cell = $(this);
        var currentValue = cell.text().trim();
        var newValue = currentValue === "Sí" ? "No" : "Sí"; // Cambiar entre "x" y "-"
        cell.text(newValue);

        // Obtener el ID del paciente y el día
        var pacienteId = cell.closest("tr").data("patient-id");
        var day = cell.index(); // Índice de la celda para obtener el día

        // Enviar una solicitud al controlador para marcar la deposición
        $.get("/deposiciones/marcar-deposicion", {idPaciente: pacienteId, dia: day, valor: newValue});
    });
});
