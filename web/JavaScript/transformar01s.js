const revisionDepos = {
    '0': 'No',
    '1': 'Sí'
};

const revisionDepos2 = {
    '0': 'abierta',
    '1': 'cerrada'
};

document.querySelectorAll('.deposiciones-index .grid-view .table tbody tr td:nth-child(5)').forEach(
    td => {
        const valor = td.textContent.trim(); //Elimina los espacios 
        td.textContent = revisionDepos[valor] || '-';
    }
);

document.querySelectorAll('.menstruaciones-index .grid-view .table tbody tr td:nth-child(4)').forEach(
    td => {
        const valor = td.textContent.trim(); //Elimina los espacios 
        td.textContent = revisionDepos[valor] || '-';
    }
);

document.querySelectorAll('.revisiones-index .grid-view .table tbody tr td:nth-child(1)').forEach(
    td => {
        const valor = td.textContent.trim(); //Elimina los espacios 
        td.textContent = revisionDepos[valor] || '-';
    }
);

document.querySelectorAll('.revisiones-index .grid-view .table tbody tr td:nth-child(2)').forEach(
    td => {
        const valor = td.textContent.trim(); //Elimina los espacios 
        td.textContent = revisionDepos[valor] || '-';
    }
);

document.querySelectorAll('.habitaciones-index .grid-view .table tbody tr td:nth-child(2)').forEach(
    td => {
        const valor = td.textContent.trim(); //Elimina los espacios 
        td.textContent = revisionDepos2[valor] || '-';
    }
);