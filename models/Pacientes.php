<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use DateTime; 

/**
 * This is the model class for table "pacientes".
 *
 * @property int $id
 * @property int|null $numeroHistoria
 * @property string|null $fechaNacimiento
 * @property string|null $nombreCompleto
 *
 * @property Camas $camas
 * @property Deposiciones[] $deposiciones
 * @property Partes[] $idPartes
 * @property Menstruaciones[] $menstruaciones
 * @property Rellenados[] $rellenados
 * @property Revisiones[] $revisiones
 */
class Pacientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pacientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numeroHistoria'], 'integer'],
            [['fechaNacimiento'], 'safe'],
            [['nombreCompleto'], 'string', 'max' => 500],
            [['sexo'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'numeroHistoria' => 'Numero Historia',
            'fechaNacimiento' => 'Fecha Nacimiento',
            'nombreCompleto' => 'Nombre Completo',
            'sexo' => 'Sexo',
        ];
    }

    /**
     * Gets query for [[Camas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCamas()
    {
        return $this->hasOne(Camas::class, ['idPacientes' => 'id']);
    }

    /**
     * Gets query for [[Deposiciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDeposiciones()
    {
        return $this->hasMany(Deposiciones::class, ['idPacientes' => 'id']);
    }

    /**
     * Gets query for [[IdPartes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdPartes()
    {
        return $this->hasMany(Partes::class, ['id' => 'idPartes'])->viaTable('rellenados', ['idPacientes' => 'id']);
    }

    /**
     * Gets query for [[Menstruaciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMenstruaciones()
    {
        return $this->hasMany(Menstruaciones::class, ['idPacientes' => 'id']);
    }

    /**
     * Gets query for [[Rellenados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRellenados()
    {
        return $this->hasMany(Rellenados::class, ['idPacientes' => 'id']);
    }

    /**
     * Gets query for [[Revisiones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRevisiones()
    {
        return $this->hasMany(Revisiones::class, ['idPacientes' => 'id']);
    }
    
    
     /**
     * Calcula la edad del paciente basada en su fecha de nacimiento.
     * 
     * @return int Edad del paciente.
     */
    public function getEdad() {
        $fechaNacimiento = new DateTime($this->fechaNacimiento);
        $hoy = new DateTime();
        $edad = $hoy->diff($fechaNacimiento);
        return $edad->y;
    }
    
    public function afterFind()
{
    parent::afterFind();
    // Formatear la fecha de nacimiento al formato deseado
    $this->fechaNacimiento = Yii::$app->formatter->asDate($this->fechaNacimiento, 'php:d-m-Y');
}

}
