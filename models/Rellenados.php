<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rellenados".
 *
 * @property int $id
 * @property int|null $idPartes
 * @property int|null $idPacientes
 * @property int|null $idAuxiliares
 * @property string|null $fecha
 * @property string|null $turno
 *
 * @property Auxiliares $idAuxiliares0
 * @property Pacientes $idPacientes0
 * @property Partes $idPartes0
 */
class Rellenados extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rellenados';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idPartes', 'idPacientes', 'idAuxiliares'], 'integer'],
            [['fecha'], 'safe'],
            [['turno'], 'string', 'max' => 500],
            [['idPartes', 'idPacientes', 'idAuxiliares'], 'unique', 'targetAttribute' => ['idPartes', 'idPacientes', 'idAuxiliares']],
            [['idPartes', 'idPacientes'], 'unique', 'targetAttribute' => ['idPartes', 'idPacientes']],
            [['idPartes', 'idAuxiliares'], 'unique', 'targetAttribute' => ['idPartes', 'idAuxiliares']],
            [['idAuxiliares'], 'exist', 'skipOnError' => true, 'targetClass' => Auxiliares::class, 'targetAttribute' => ['idAuxiliares' => 'id']],
            [['idPacientes'], 'exist', 'skipOnError' => true, 'targetClass' => Pacientes::class, 'targetAttribute' => ['idPacientes' => 'id']],
            [['idPartes'], 'exist', 'skipOnError' => true, 'targetClass' => Partes::class, 'targetAttribute' => ['idPartes' => 'id']],
            [['idPartes', 'idPacientes'], 'validateUniquePerDay'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idPartes' => 'Id Partes',
            'idPacientes' => 'Id Pacientes',
            'idAuxiliares' => 'Auxiliares',
            'fecha' => 'Fecha',
            'turno' => 'Turno',
        ];
    }

    /**
     * Gets query for [[IdAuxiliares0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdAuxiliares0()
    {
        return $this->hasOne(Auxiliares::class, ['id' => 'idAuxiliares']);
    }

    /**
     * Gets query for [[IdPacientes0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdPacientes0()
    {
        return $this->hasOne(Pacientes::class, ['id' => 'idPacientes']);
    }

    /**
     * Gets query for [[IdPartes0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdPartes0()
    {
        return $this->hasOne(Partes::class, ['id' => 'idPartes']);
    }
    
    /**
     * Custom validation rule to ensure only one record per day per paciente.
     */
    public function validateUniquePerDay($attribute, $params)
    {
        // Formar la fecha sin considerar la hora
        $fecha = Yii::$app->formatter->asDate($this->fecha, 'php:Y-m-d');
        
        // Buscar si existe otro registro para el mismo paciente en la misma fecha y turno
        $existingRecord = static::findOne(['idPacientes' => $this->idPacientes, 'fecha' => $fecha, 'turno' => $this->turno]);
        
        // Si se encuentra un registro existente, y no es el mismo que el actual, añadir un error de validación
        if ($existingRecord && $existingRecord->id != $this->id) {
            $this->addError($attribute, 'Ya existe un registro para este paciente en el mismo turno y día.');
        }
    }
    
    /**
     * After find callback to format the fecha attribute.
     */
    public function afterFind()
    {
        parent::afterFind();
        
        // Formatear la fecha al formato deseado
        $this->fecha = Yii::$app->formatter->asDate($this->fecha, 'php:d-m-Y');
    }
}
