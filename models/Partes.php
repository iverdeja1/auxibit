<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "partes".
 *
 * @property int $id
 * @property string|null $descripcion
 *
 * @property Auxiliares[] $idAuxiliares
 * @property Pacientes[] $idPacientes
 * @property Rellenados[] $rellenados
 */
class Partes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'partes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion'], 'string'],
            [['descripcion'], 'trim'], // Eliminar espacios en blanco alrededor del texto
            [['descripcion'], 'required', 'message' => 'La descripción es requerida.'], // La descripción no puede estar vacía
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'descripcion' => 'Observaciones',
        ];
    }

    /**
     * Gets query for [[IdAuxiliares]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdAuxiliares()
    {
        return $this->hasMany(Auxiliares::class, ['id' => 'idAuxiliares'])->viaTable('rellenados', ['idPartes' => 'id']);
    }

    /**
     * Gets query for [[IdPacientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdPacientes()
    {
        return $this->hasMany(Pacientes::class, ['id' => 'idPacientes'])->viaTable('rellenados', ['idPartes' => 'id']);
    }

    /**
     * Gets query for [[Rellenados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRellenados()
    {
        return $this->hasMany(Rellenados::class, ['idPartes' => 'id']);
    }
    public function getPaciente()
{
    return $this->hasOne(Pacientes::className(), ['id' => 'idaciente']); // Reemplaza 'id_paciente' con el nombre correcto de la columna de clave foránea en la tabla 'partes'
}

}
