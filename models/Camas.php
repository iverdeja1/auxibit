<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "camas".
 *
 * @property int $id
 * @property string|null $letraCama
 * @property int|null $numHabitacion
 * @property int|null $ocupacion
 * @property int|null $idPacientes
 *
 * @property Pacientes $idPacientes0
 * @property Habitaciones $numHabitacion0
 */
class Camas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'camas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numHabitacion', 'ocupacion', 'idPacientes'], 'integer'],
            [['letraCama'], 'string', 'max' => 1],
            [['letraCama', 'numHabitacion'], 'unique', 'targetAttribute' => ['letraCama', 'numHabitacion']],
            [['idPacientes'], 'unique'],
            [['numHabitacion'], 'exist', 'skipOnError' => true, 'targetClass' => Habitaciones::class, 'targetAttribute' => ['numHabitacion' => 'id']],
            [['idPacientes'], 'exist', 'skipOnError' => true, 'targetClass' => Pacientes::class, 'targetAttribute' => ['idPacientes' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'letraCama' => 'Letra Cama',
            'numHabitacion' => 'Num Habitacion',
            'ocupacion' => 'Ocupacion',
            'idPacientes' => 'Id Pacientes',
        ];
    }

    /**
     * Gets query for [[IdPacientes0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdPacientes0()
    {
        return $this->hasOne(Pacientes::class, ['id' => 'idPacientes']);
    }

    /**
     * Gets query for [[NumHabitacion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNumHabitacion0()
    {
        return $this->hasOne(Habitaciones::class, ['id' => 'numHabitacion']);
    }
}
