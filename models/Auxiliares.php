<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "auxiliares".
 *
 * @property int $id
 * @property string|null $nombreAuxiliar
 *
 * @property Partes[] $idPartes
 * @property Rellenados[] $rellenados
 * @property Telefonos[] $telefonos
 */
class Auxiliares extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'auxiliares';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombreAuxiliar'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombreAuxiliar' => 'Nombre Auxiliar',
        ];
    }

    /**
     * Gets query for [[IdPartes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdPartes()
    {
        return $this->hasMany(Partes::class, ['id' => 'idPartes'])->viaTable('rellenados', ['idAuxiliares' => 'id']);
    }

    /**
     * Gets query for [[Rellenados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRellenados()
    {
        return $this->hasMany(Rellenados::class, ['idAuxiliares' => 'id']);
    }

    /**
     * Gets query for [[Telefonos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTelefonos()
    {
        return $this->hasMany(Telefonos::class, ['idAuxiliares' => 'id']);
    }
}
