<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "telefonos".
 *
 * @property int $id
 * @property int|null $idAuxiliares
 * @property string|null $telefono
 *
 * @property Auxiliares $idAuxiliares0
 */
class Telefonos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'telefonos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idAuxiliares'], 'integer'],
            [['telefono'], 'string', 'max' => 500],
            [['idAuxiliares', 'telefono'], 'unique', 'targetAttribute' => ['idAuxiliares', 'telefono']],
            [['idAuxiliares'], 'exist', 'skipOnError' => true, 'targetClass' => Auxiliares::class, 'targetAttribute' => ['idAuxiliares' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idAuxiliares' => 'Id Auxiliares',
            'telefono' => 'Telefono',
        ];
    }

    /**
     * Gets query for [[IdAuxiliares0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdAuxiliares0()
    {
        return $this->hasOne(Auxiliares::class, ['id' => 'idAuxiliares']);
    }
}
