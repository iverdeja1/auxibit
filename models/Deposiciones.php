<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "deposiciones".
 *
 * @property int $id
 * @property string|null $turno
 * @property string|null $fecha
 * @property int|null $positivo
 * @property int|null $idPacientes
 *
 * @property Pacientes $idPacientes0
 */
class Deposiciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'deposiciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['positivo', 'idPacientes'], 'integer'],
            [['turno'], 'string', 'max' => 500],
            [['idPacientes'], 'exist', 'skipOnError' => true, 'targetClass' => Pacientes::class, 'targetAttribute' => ['idPacientes' => 'id']],
            [['idPacientes', 'fecha', 'turno', 'positivo'], 'required'],
            ['fecha', 'uniqueTurno'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'turno' => 'Turno',
            'fecha' => 'Fecha',
            'positivo' => 'Realiza deposición',
            'idPacientes' => 'Id Pacientes',
        ];
    }

    /**
     * Gets query for [[IdPacientes0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdPacientes0()
    {
        return $this->hasOne(Pacientes::class, ['id' => 'idPacientes']);
    }
    
    /**
     * Custom validation rule to ensure only one record per turno.
     */
public function uniqueTurno($attribute, $params)
    {
        // Buscar si ya existe un registro con el mismo turno y fecha para el mismo paciente
        $existingRecord = static::find()
            ->where(['fecha' => $this->fecha, 'turno' => $this->turno, 'idPacientes' => $this->idPacientes])
            ->one();
        
        // Si existe un registro y no es el mismo que se está editando, agregar un error
        if ($existingRecord && $existingRecord->id != $this->id) {
            $this->addError($attribute, 'Ya existe un registro para este turno.');
        }
    }
    /**
     * After find callback to format the fecha attribute.
     */
    public function afterFind()
    {
        parent::afterFind();
        // Format the fecha attribute after retrieving it from the database
        $this->fecha = Yii::$app->formatter->asDate($this->fecha, 'php:d-m-Y');
    }
}
