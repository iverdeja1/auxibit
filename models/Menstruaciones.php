<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "menstruaciones".
 *
 * @property int $id
 * @property int|null $positivo
 * @property string|null $fecha
 * @property int|null $idPacientes
 *
 * @property Pacientes $idPacientes0
 */
class Menstruaciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menstruaciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['positivo', 'idPacientes'], 'integer'],
            [['fecha'], 'safe'],
            [['idPacientes'], 'exist', 'skipOnError' => true, 'targetClass' => Pacientes::class, 'targetAttribute' => ['idPacientes' => 'id']],
            ['fecha', 'uniqueDate'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'positivo' => 'Presenta sangrado',
            'fecha' => 'Fecha',
            'idPacientes' => 'Id Pacientes',
        ];
    }

    /**
     * Gets query for [[IdPacientes0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdPacientes0()
    {
        return $this->hasOne(Pacientes::class, ['id' => 'idPacientes']);
    }
    
    /**
     * Custom validation rule to ensure only one record per day.
     */
    public function uniqueDate($attribute, $params)
    {
        // Find if there is already a record with the same fecha for the same paciente
        $existingRecord = static::findOne(['fecha' => $this->fecha, 'idPacientes' => $this->idPacientes]);
        
        // If there is an existing record and it's not the same as the current one, add an error
        if ($existingRecord && $existingRecord->id != $this->id) {
            $this->addError($attribute, 'Ya existe una menstruación registrada para este día.');
        }
    }
    
    /**
     * After find callback to format the fecha attribute.
     */
    public function afterFind()
    {
        parent::afterFind();

        // Format the fecha attribute after retrieving it from the database
        $this->fecha = Yii::$app->formatter->asDate($this->fecha, 'php:d-m-Y');
    }
}
