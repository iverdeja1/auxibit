<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "habitaciones".
 *
 * @property int $id
 * @property int|null $numeroHabitacion
 * @property int|null $estadoPuerta
 *
 * @property Camas[] $camas
 */
class Habitaciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'habitaciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numeroHabitacion', 'estadoPuerta'], 'integer'],
            [['numeroHabitacion'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'numeroHabitacion' => 'Numero Habitacion',
            'estadoPuerta' => 'Estado Puerta',
        ];
    }

    /**
     * Gets query for [[Camas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCamas()
    {
        return $this->hasMany(Camas::class, ['numHabitacion' => 'id']);
    }
}
